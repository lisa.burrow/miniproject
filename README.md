# Description

Simple Hello World app

# How to run

In Visual studio  
In file listing
Right Click on index.html 
    > Open with live server

# How to install

Clone from Gitlab project ....

# How to test

to do

# How I have set this project up

1) Created index.html 
2) Added some css - helloWorld.css
3) Added a js file - helloWorld.js

To be able to TDD this using something like Jest need to install node (used to run to get to 3rd party code)

In Git bash:

4) Type 'npm init' to set up the environment, creates the package.json and other folders.

Now install Jest

5) Type 'npm install --save-dev jest' (-dev means only in dev environment as you don't want Jest to go live!)

Running tests

6) Type Jest


